var express = require('express');
var staticOptions = {
    index: true
}
express.static('static', staticOptions);
var app = express();

var server_port = process.env.OPENSHIFT_NODEJS_PORT;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP;

app.use(express.static('static'));
app.get('/', function (req, res) {
    res.send("Hi");
    res.redirect('index.html');
});
app.listen(server_port, function () {
    console.log('Example app listening on port ' + server_port + '!');
});