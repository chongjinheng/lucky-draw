function draw() {

  var minTextBox = document.getElementById("min");
  var maxTextBox = document.getElementById("max");

  // showOrHideInput(false);

  // var min = parseFloat(minTextBox.value);
  // var max = parseFloat(maxTextBox.value);
  var min = 1;
  var max = 99;
  var totalNumber = max - min + 1;

  if (validInput(min, max)) {
    var number;
    do {
      number = generateRandom(min, max);
    } while (drawed(number) && usedList.length < totalNumber);

    if (usedList.length >= totalNumber) {
      alert("Lucky Draw Over!");
    } else {
      usedList.push(number);

      var displayNum;
      if (number < 10) {
        displayNum = "000" + number;
      } else if (number < 100) {
        displayNum = "00" + number;
      } else if (number < 1000) {
        displayNum = "0" + number;
      } else {
        displayNum = number;
      }
      alert(displayNum);
      // document.getElementById("draw-number").innerHTML = displayNum;
    }
  }
}

function validInput(min, max) {
  var valid = false;
  if (isNaN(min) || isNaN(max)) {
    alert("Please input only numbers");
  } else if (min >= max) {
    alert("min is more than max?");
  } else {
    valid = true;
  }
  if (!valid) {
    showOrHideInput(true);
  }
  return valid;
}

function showOrHideInput(showOrNot) {

  var display = showOrNot == true ? "block" : "none";

  document.getElementById("min-label").style.display = display;
  document.getElementById("max-label").style.display = display;
  document.getElementById("min").style.display = display;
  document.getElementById("max").style.display = display;

  if (display === "block") {
    document.getElementById("min").value = "";
    document.getElementById("max").value = "";
  }
}

function drawed(number) {
  for (var i = 0; i < usedList.length; i++) {
    if (usedList[i] === number) {
      return true;
    }
  }
  return false;
}

function generateRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function redoLuckyDraw() {
  usedList = [];
  document.getElementById("draw-number").innerHTML = "";
  showOrHideInput(true);
}

function playDraw() {
  var audio = document.getElementById("draw");
  audio.volume = 0.5;
  audio.play();

}

function playWin() {
  var previousAudio = document.getElementById("draw");
  previousAudio.pause();
  previousAudio.load();
  var audio = document.getElementById("win");
  audio.volume = 0.5;
  audio.play();
}

function login() {
  this.key = document.getElementById("loginId").value;
  window.location.replace("index.html");
}