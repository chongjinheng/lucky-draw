var usedList = [];
var highest_unit_int = 4;
var numberOfSlotSpinned = 0;
var slot1 = -1;
var slot2 = -1;
var slot3 = -1;

var height_slot_number = '100';
highest_unit_int += 1;

function go(hundreds, tens, units) {
    addSlots($("#slots_units .number-wrapper"), 10);
    addSlots($("#slots_units .number-wrapper"), 10);
    moveSlots($("#slots_units .number-wrapper"), units, 3);
    addSlots($("#slots_tens .number-wrapper"), 10);
    addSlots($("#slots_tens .number-wrapper"), 10);
    moveSlots($("#slots_tens .number-wrapper"), tens, 2);
    addSlots($("#slots_hundreds .number-wrapper"), highest_unit_int);
    addSlots($("#slots_hundreds .number-wrapper"), highest_unit_int);
    moveSlots($("#slots_hundreds .number-wrapper"), hundreds, 1);
}

var key;

$(document).ready(function () {
    addSlots($("#slots_units .number-wrapper"), 10);
    addSlots($("#slots_tens .number-wrapper"), 10);
    addSlots($("#slots_hundreds .number-wrapper"), highest_unit_int);
    $('#arm').click(function (e) {
        if (key === "wannasleep") {
            playDraw();
            var arm = $(this).addClass('clicked');
            delay = setTimeout(function () {
                arm.removeClass('clicked');
            }, 500);
            e.preventDefault();
            go(Math.floor(Math.random() * 10), Math.floor(Math.random() * 10), Math.floor(Math.random() * 10));
        }
    });
    var login = prompt("Please login");
    key = login;
    console.log(key);
});



function addSlots(jqo, highestNo) {
    for (var i = 0; i < highestNo; i++) {
        jqo.append("<div class='slot'>" + i + "</div>");
    }
}


function moveSlots(jqo, num, slotNo) {
    if (key === "wannasleep") {
        var time = 6500;
        var number = num;
        time += Math.round(Math.random() * 1000);
        jqo.stop(true, true);

        var num_slot = Math.round((jqo.find('.slot').length) / 20);

        var margin_top = ((num_slot - 1) * (height_slot_number * 10)) + (num * height_slot_number);

        if (slotNo == 1) {
            slot1 = calculateExactValue(margin_top, slotNo);
        }
        if (slotNo == 2) {
            slot2 = calculateExactValue(margin_top, slotNo);
        }
        if (slotNo == 3) {
            slot3 = calculateExactValue(margin_top, slotNo);
        }

        if (slot1 != -1 && slot2 != -1 && slot3 != -1) {

            var noInString = slot1.toString() + slot2.toString() + slot3.toString();
            console.log(noInString);
            var no = parseInt(noInString);
            checkUsedOrNot(no);
            slot1 = -1;
            slot2 = -1;
            slot3 = -1;
        }


        jqo.animate({
            "margin-top": "-" + margin_top + "px"
        }, {
            'duration': time,
            'easing': "easeOutElastic"
        });
        setTimeout(function () {
            playWin();
        }, 3800);

    }
}

function calculateExactValue(margin_top, slotNo) {
    var no;

    // console.log(margin_top);
    if (slotNo != 1) {
        no = (margin_top / 100) % 10;
    } else {
        no = (margin_top / 100) % highest_unit_int;
    }
    return no;
}

function checkUsedOrNot(number) {
    if (usedList.length === 0) {
        usedList.push(number);
    } else {
        var refUsedList = usedList.slice();
        for (var i = 0; i < refUsedList.length; i++) {
            if (refUsedList[i] === number) {
                console.log(usedList[i]);
                console.log(number);
                setTimeout(function () {
                    $('#arm').click();
                }, 500);
                return false;
            }
        }
        usedList.push(number);
    }
    return false;
}